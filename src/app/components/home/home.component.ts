import { Component } from '@angular/core';
import { SpotiAppService } from '../../services/spoti-app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [],
})
export class HomeComponent {

  newCanciones: any[] = [];
  loading: boolean;
  error: boolean;
  mensajeError: string;

  constructor(private spotiApp: SpotiAppService) {
    this.loading = true;
    this.error = true;
    this.spotiApp.getNewReleases()
      .subscribe((data: any) => {
        this.newCanciones = data;
        this.loading = false;
      }, (errorServicio) => {
        this.loading = false;
        this.error = false;
        console.log(errorServicio);
        console.log(errorServicio.error.error.message);
        this.mensajeError = errorServicio.error.error.message ;
      });
  }
}

