import { Component } from '@angular/core';
import { isEmpty } from 'rxjs/operators';
import { SpotiAppService } from '../../services/spoti-app.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
})
export class SearchComponent {
  artistas: any[] = [];
  loading: boolean;

  constructor(private spotiApp: SpotiAppService) { }

  buscar(termino: string) {
    if (termino === ''){ this.loading = false; } else{
    console.log(termino);
    this.loading = true;
    this.spotiApp.getArtistas(termino)
      .subscribe((data: any) => {
        console.log(data);
        this.artistas = data;
        this.loading = false;
      });
    }
  }
}
