import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpotiAppService } from '../../services/spoti-app.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
})
export class ArtistaComponent {
  artista: any = {};
  loading: boolean;
  topTrackss: any[] = [];

  constructor(private router: ActivatedRoute, private spotiApp: SpotiAppService) {
    this.loading = true;
    this.router.params.subscribe(params => {
      this.getArtista(params['id']);
      this.getTopTracks(params['id']);
    });
  }
  getArtista(id: string) {
    this.loading = true;
    this.spotiApp.getArtista(id)
      .subscribe(artista => {
        console.log(artista);
        this.artista = artista;
        this.loading = false;
      });
  }
  getTopTracks(id: string) {
    this.spotiApp.getTopTracks(id).subscribe(topTracks => {
      console.log(topTracks);
      this.topTrackss = topTracks;
    });

  }
}
